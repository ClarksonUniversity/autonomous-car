package autocar;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import murphycd.engine.core.ChApplication;
import murphycd.engine.core.ChApplication.Attribute;
import murphycd.engine.core.ChApplication.Flag;

public class Launcher {

	private static String help = "";
	static {
		help += "Usage\n" //
		        + "-v   --verbose          Increase debug output\n" //
		        + "     --ups=rate         Integer rate for updates per second (simulation tick)\n" //
		        + "                        integer greater than 2\n" //
		        + "     --window=token     Token format: WIDTHxHEIGHT (integers)," //
		        + "                        no less than 200x200.";
	}

	public static void main(String[] args) {

		try {
			Map<String, List<String>> parameterMap = processCommandLineArguments(args);
			prepareSimEnvironment(parameterMap);
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
			System.out.println("Try --help for more info");
			return;
		}

		// Launch App
		App sim = new App();
		sim.start();
	}

	/**
	 * Process short-form (-) and long-form (--) command line arguments.
	 * 
	 * @param args
	 *            from command line
	 * @return {@link java.util.Map} where keys are short or long parameters,
	 *         values are any associated options
	 * @throws RuntimeException
	 *             command line arguments cannot be processed
	 */
	static final Map<String, List<String>> processCommandLineArguments(String[] args) throws RuntimeException {

		final Map<String, List<String>> params = new HashMap<>();
		List<String> options = null;
		for (int i = 0; i < args.length; i++) {
			final String a = args[i];

			// begin short (-) or long (--) parameter
			if (a.charAt(0) == '-') {
				if (a.length() < 2) {
					throw new RuntimeException("Invalid argument " + a);
				}

				options = new ArrayList<>();

				// check for long (--) parameter
				if (a.charAt(1) == '-') {
					if (a.length() < 3) {
						throw new RuntimeException("Invalid argument " + a);
					}

					int beginLongOption = a.indexOf('=') + 1;
					if (beginLongOption > 2) {
						options.add(a.substring(beginLongOption));
						params.put(a.substring(2, beginLongOption - 1), options);
					} else {
						options = new ArrayList<>();
						params.put(a.substring(2), options);
					}
					// long arguments should specify no space-separated options
					options = null;

				} else {
					// one or more short parameter with possible future
					// space-separated options
					for (char letter : a.substring(1).toCharArray()) {
						params.put("" + letter, options);
					}
				}
			} else if (options != null) {
				// continue short (-) parameter
				options.add(a);
			} else {
				// illegal parameter state
				throw new RuntimeException("Invalid argument " + a);
			}
		}
		return params;
	}

	/**
	 * References parsed command line arguments to prepare the Simulation state,
	 * namely {@link ChApplication.Flag} and {@link ChApplication.Attribute}.
	 * 
	 * @param params
	 *            command line arguments
	 */
	static void prepareSimEnvironment(Map<String, List<String>> params) throws RuntimeException {

		if (params.containsKey("help")) {
			System.out.println(help);
			System.exit(0);
		}

		Flag.verbose = params.containsKey("v") || params.containsKey("verbose");
		Flag.noborder = true;
		Attribute.title = "Autonomous Car";

		if (params.containsKey("window")) {
			try {
				String size = params.get("window").get(0);
				String width = size.substring(0, size.indexOf('x'));
				String height = size.substring(size.indexOf('x') + 1);

				Attribute.width = (int) (Integer.parseInt(width) / Attribute.scale + 0.5);
				Attribute.height = (int) (Integer.parseInt(height) / Attribute.scale + 0.5);
				if (Attribute.getScaledWidth() < 200 || Attribute.getScaledHeight() < 200) {
					throw new NumberFormatException();
				}
			} catch (NumberFormatException | IndexOutOfBoundsException exception) {
				throw new RuntimeException(
				        "Parameter window=WIDTHxHEIGHT must contain integers WIDTH and HEIGHT where the total dimension is at least 200x200.");
			}
		} else {
			// borderless window (default)
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Attribute.width = (int) (screenSize.getWidth() / Attribute.scale + 0.5);
			Attribute.height = (int) (screenSize.getHeight() / Attribute.scale + 0.5);
		}

		if (params.containsKey("ups")) {
			try {
				Attribute.updatesPerSecond = Integer.parseInt(params.get("ups").get(0));
				if (Attribute.updatesPerSecond < 2) {
					throw new NumberFormatException();
				}
			} catch (NumberFormatException exception) {
				throw new RuntimeException("Updates per second must be specified as an integer greater than 2");
			}
		} else {
			// simulation default
			Attribute.updatesPerSecond = 30;
		}

		Attribute.backgroundColor = 0xE7E7E7; // white

	}
}
