package autocar;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.NoninvertibleTransformException;

import autocar.navigation.AutonomousNavigation;
import autocar.objects.Car;
import autocar.objects.Entity;
import autocar.objects.Pylon;
import murphycd.engine.core.ChApplication;
import murphycd.engine.util.Camera;
import murphycd.engine.util.DoubleGraphics2D;
import murphycd.engine.util.Drawable;
import murphycd.engine.util.Input.MouseInputListener;

public class World implements Drawable, MouseInputListener {

	private AppData data;

	private Point mouseXY;
	private Point worldXY = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);

	private Color fieldBackground = new Color(0xCFCFCF);
	private Car car;

	public World() {
		data = AppData.getInstance();

		double carWidth = 14.97;
		double carHeight = 7.5;
		Color carColor = Color.DARK_GRAY;

		// car initial position (change these)
		double carPosX = 10.0;
		double carPosY = -50.0;
		double carRot = -70.0 * (Math.PI / 180.0);

		car = new Car(carPosX, carPosY, carWidth, carHeight, carRot, carColor);
		car.setDrawState(State.OPAQUE);

		double corner = 144 / 2;
		data.entities.put("cornerPylon1", new Pylon(corner, corner, 2.0, Color.ORANGE));
		data.entities.put("cornerPylon2", new Pylon(corner, -corner, 2.0, Color.ORANGE.darker()));
		data.entities.put("cornerPylon3", new Pylon(-corner, corner, 2.0, Color.ORANGE));
		data.entities.put("cornerPylon4", new Pylon(-corner, -corner, 2.0, Color.ORANGE.brighter()));
	}

	public void update() {
		if (mouseXY != null) {
			// map mouse screen coordinates to world coordinates
			try {
				worldXY.setLocation(Camera.transformPoint(mouseXY));
			} catch (NoninvertibleTransformException e) {
				e.printStackTrace();
			}

			if (AppData.manual_car_control) {
				double screenWidth = ChApplication.Attribute.getScaledWidth();
				double screenHeight = ChApplication.Attribute.getScaledHeight();
				if (mouseXY.x > screenWidth * (0.5 + AppData.manual_control_deadzone / 2)
				        || mouseXY.x < screenWidth * (0.5 - AppData.manual_control_deadzone / 2)) {
					car.control
					        .setWheelAngle((mouseXY.x - screenWidth / 2) / (screenWidth / 2) * AppData.MAX_WHEEL_ANGLE);
				} else {
					car.control.setWheelAngle(0);
				}
				if (mouseXY.y < screenHeight * (1 - AppData.manual_control_deadzone)) {
					car.control.setSpeed((screenHeight * (1 - AppData.manual_control_deadzone) - mouseXY.y)
					        / (screenHeight * (1 - AppData.manual_control_deadzone)) * AppData.MAX_CAR_SPEED);
				} else {
					car.control.setSpeed(0);
				}
			} else {
				AutonomousNavigation.nav(car.sensors, car.control);
			}
		}

		car.update();
	}

	@Override
	public void draw(Graphics2D g2) {
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// draw field
		g2.setColor(fieldBackground);
		DoubleGraphics2D.fillRect(g2, -144 / 2.0, -144 / 2.0, 144, 144);

		// draw 1-foot guide (outer)
		DoubleGraphics2D.drawLine(g2, -144 / 2.0, -144 / 2.0 - 12, 144 / 2.0, -144 / 2.0 - 12);
		DoubleGraphics2D.drawLine(g2, 144 / 2.0 + 12, -144 / 2.0, 144 / 2.0 + 12, 144 / 2.0);
		DoubleGraphics2D.drawLine(g2, 144 / 2.0, 144 / 2.0 + 12, -144 / 2.0, 144 / 2.0 + 12);
		DoubleGraphics2D.drawLine(g2, -144 / 2.0 - 12, 144 / 2.0, -144 / 2.0 - 12, -144 / 2.0);

		// draw 1-foot guide (inner)
		g2.setColor(new Color(ChApplication.Attribute.backgroundColor));
		DoubleGraphics2D.drawLine(g2, -144 / 2.0 + 12, -144 / 2.0 + 12, 144 / 2.0 - 12, -144 / 2.0 + 12);
		DoubleGraphics2D.drawLine(g2, 144 / 2.0 - 12, -144 / 2.0 + 12, 144 / 2.0 - 12, 144 / 2.0 - 12);
		DoubleGraphics2D.drawLine(g2, 144 / 2.0 - 12, 144 / 2.0 - 12, -144 / 2.0 + 12, 144 / 2.0 - 12);
		DoubleGraphics2D.drawLine(g2, -144 / 2.0 + 12, 144 / 2.0 - 12, -144 / 2.0 + 12, -144 / 2.0 + 12);

		// origin marker
		// drawDebugCross(g2, 0, 0, 0, 10, Color.BLACK);

		car.draw(g2);

		for (Entity entity : data.entities.values()) {
			entity.draw(g2);
		}
	}

	@Override
	public boolean mouseClicked(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseEntered(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mousePressed(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseReleased(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseMoved(MouseEvent event) {
		mouseXY = event.getPoint();
		return false;
	}

	@Override
	public boolean mouseDragged(MouseEvent event) {
		return false;
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent event) {
		return false;
	}
}
