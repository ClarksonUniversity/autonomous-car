package autocar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import autocar.ui.Gui;
import murphycd.engine.core.ChApplicationAdapter;
import murphycd.engine.util.ButtonClickHandler;
import murphycd.engine.util.Camera;
import murphycd.engine.util.Input;

public class App extends ChApplicationAdapter implements ButtonClickHandler {

	private static final long serialVersionUID = 1L;

	private Input input;
	private Gui gui;
	private World world;
	private Camera camera;

	protected void init() {

		input = new Input(this);

		gui = new Gui(this);
		world = new World();
		camera = new Camera(this);
		camera.setInitScale(7.0);

		// gui receives events before camera
		input.addMouseInputListener(gui);
		input.addMouseInputListener(camera);
		input.addMouseInputListener(world);

		Flag.visible = true;
	}

	protected void update() {
		world.update();
	}

	protected void draw(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;

		// project world space to screen space using current pan and zoom
		camera.setProjection(g);

		// draw world
		world.draw(g2);

		// unproject for UI elements
		camera.resetProjection(g);
		gui.draw(g2);

		// window border
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, (int) Attribute.getScaledWidth() - 1, (int) Attribute.getScaledHeight() - 1);
	}

	@Override
	public void buttonClicked(ButtonClickEvent event) {
		switch (event.source) {
			case AppData.BUTTON_EXIT:
				stop();
				break;
			default:
				// do nothing
		}
	}
}
