package autocar.navigation;

import java.util.List;

import autocar.objects.Car.Control;
import autocar.objects.Car.Sensors;
import autocar.ui.elements.Camera;
import autocar.ui.elements.Camera.Block;

public class AutonomousNavigation {

	private static enum NavState {
		DISCOVER_FIRST, DISCOVER, TRACK_FIRST, TRACK, TURN_AWAY, TURN_TOWARD
	}

	private static NavState state = NavState.DISCOVER_FIRST;

	private static final double DEG2RAD = Math.PI / 180.0;

	private static final double TURN_ANGLE = 15 * DEG2RAD;
	private static final double TURN_DECISION_DISTANCE = 24;

	public static void nav(Sensors sensors, Control control) {

		// get sensor data
		List<Block> blocks = sensors.getCameraBlocks();
		int distance = sensors.getDistanceMeasurement();

		if (distance < 8) {
			control.setSpeed(0);
		}

		// state machine
		switch (state) {

			case DISCOVER_FIRST:
			case DISCOVER:

				if (blocks.isEmpty()) {
					control.setSpeed(18);
					if (state == NavState.DISCOVER_FIRST) {
						control.setWheelAngle(45 * DEG2RAD);
					} else {
						control.setWheelAngle(25 * DEG2RAD);
					}
				} else {
					state = NavState.TRACK;
				}

				break;

			case TRACK:

				control.setSpeed(18);

				if (distance < TURN_DECISION_DISTANCE) {
					state = NavState.TURN_AWAY;
					control.setWheelAngle(TURN_ANGLE);
				} else {
					if (blocks.isEmpty()) {
						state = NavState.DISCOVER;
					} else {
						// track closest block
						Block closestBlock = blocks.get(0);
						double declination = closestBlock.getCenterX() * Camera.carCamFov - Camera.carCamFov / 2.0;
						control.setWheelAngle(declination * 2);
					}
				}

				break;

			case TURN_AWAY:

				control.setSpeed(18);
				control.setWheelAngle(TURN_ANGLE);

				if (blocks.isEmpty()) {
					state = NavState.TURN_TOWARD;
				}

				break;

			case TURN_TOWARD:

				control.setSpeed(18);
				control.setWheelAngle(TURN_ANGLE);

				if (!blocks.isEmpty()) {
					state = NavState.TRACK;
				}

				break;

			default:
				state = NavState.TURN_AWAY;
				break;
		}

	}

}
