package autocar.objects;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import murphycd.engine.util.Drawable;

public abstract class Entity implements Drawable {
	
	protected double centerX, centerY;
	protected double width, height;
	protected double rot;
	protected Color color;
	

	protected State drawState;

	protected Entity() {
		this.centerX = 0.0;
		this.centerY = 0.0;
		this.rot = 0.0;
		this.color = Color.BLACK;

		drawState = State.HIDDEN;
	}
	
	protected Entity(double posX, double posY, double width, double height, double rot, Color color) {
		this.centerX = posX;
		this.centerY = posY;
		this.width = width;
		this.height = height;
		this.rot = rot;
		this.color = color;

		drawState = State.HIDDEN;
	}

	@Override
	public abstract void draw(Graphics2D g2);

	public void setDrawState(State drawState) {
		this.drawState = drawState;
	}

	public Rectangle2D.Double getBounds() {
		return new Rectangle2D.Double(centerX - 0.5 * width, centerY - 0.5 * height, width, height);
	}

	public Color getColor() {
		return color;
	}

}
