package autocar.objects;

import java.awt.Color;
import java.awt.Graphics2D;

import murphycd.engine.util.DoubleGraphics2D;

public class Pylon extends Entity {

	public Pylon(double xPos, double yPos, double radius, Color color) {
		super(xPos, yPos, radius * 2, radius * 2, 0.0, color);
	}

	@Override
	public void draw(Graphics2D g2) {
		g2.setColor(color);
		DoubleGraphics2D.fillOval(g2, centerX - width / 2.0, centerY - height / 2.0, width, height);
	}

}
