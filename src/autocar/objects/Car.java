package autocar.objects;

import static murphycd.engine.util.DoubleGraphics2D.drawDebugCross;
import static murphycd.engine.util.DoubleGraphics2D.fillRect;
import static murphycd.engine.util.DoubleGraphics2D.translate;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import autocar.AppData;
import autocar.ui.elements.Camera;
import autocar.ui.elements.Camera.Block;
import murphycd.engine.core.ChApplication;
import murphycd.engine.util.ChMath;
import murphycd.engine.util.DoubleGraphics2D;

public class Car extends Entity {

	public static Camera camera = new Camera(
	        new Rectangle(20, 20, Camera.carCameraViewportWidth, Camera.carCameraViewportHeight));

	private static DistanceSensor distanceSensor = new DistanceSensor();

	public Control control = new Control();
	public Sensors sensors = new Sensors();

	/**
	 * A class for interfacing with a car's controls.
	 * <ul>
	 * <li>{@link Control#setSpeed(double)} <i>sets the rear wheel rotation
	 * speed in inches per second. Assume no slip and an instantaneous response
	 * time.</i></li>
	 * <li>{@link Control#setWheelAngle(double)} <i>sets the front wheel
	 * steering angle in radians. Assume an instantaneous response time</i></li>
	 * </ul>
	 */
	public class Control {
		public void setSpeed(double newSpeed) {
			speed = newSpeed;
		}

		public void setWheelAngle(double wheelAngle) {
			// bound angle
			if (wheelAngle > Tire.MAX_WHEEL_ANGLE) {
				wheelAngle = Tire.MAX_WHEEL_ANGLE;
			} else if (wheelAngle < -Tire.MAX_WHEEL_ANGLE) {
				wheelAngle = -Tire.MAX_WHEEL_ANGLE;
			}
			// update angle
			for (Tire tire : frontTires) {
				tire.tireRot = wheelAngle;
			}
		}
	}

	/**
	 * A class for interfacing with a car's sensors.
	 * <ul>
	 * <li>{@link Sensors#getCameraBlocks()} <i>returns {@link Block}s visible
	 * to the camera, beginning with the largest blocks.</i></li>
	 * <li>{@link Sensors#getDistanceMeasurement()} <i>returns the distance
	 * value (in inches) measured by forward-facing ultrasonic sensor.</i></li>
	 * </ul>
	 */
	public class Sensors {

		public List<Block> getCameraBlocks() {
			List<Block> blocks = camera.getCameraBlocksReversed();
			Collections.reverse(blocks);
			return blocks;
		}

		/** Inches */
		public int getDistanceMeasurement() {
			return distanceSensor.getDistanceMeasurement();
		}
	}

	class Box {
		double boxCenterX, boxCenterY, boxWidth, boxHeight;

		/** Values are normalized to 1.0 */
		public Box(double boxCenterX, double boxCenterY, double boxWidth, double boxHeight) {
			this.boxCenterX = boxCenterX * width;
			this.boxCenterY = boxCenterY * height;
			this.boxWidth = boxWidth * width;
			this.boxHeight = boxHeight * height;
		}

		public Point2D.Double calculatePosInWorld() {
			return ChMath.rotatePoint(boxCenterX, boxCenterY, centerX, centerY, rot);
		}

		public Point2D.Double calculateAxlePosInWorld() {
			return ChMath.rotatePoint(boxCenterX, 0, centerX, centerY, rot);
		}

		public void draw(Graphics2D g2) {
			fillRect(g2, boxCenterX - boxWidth / 2, boxCenterY - boxHeight / 2, boxWidth, boxHeight);
		}
	}

	class Tire extends Box {

		final static double MAX_WHEEL_ANGLE = AppData.MAX_WHEEL_ANGLE;

		double tireRot;

		/** Values are normalized to 1.0 */
		public Tire(double centerX, double centerY, double width, double height) {
			super(centerX, centerY, width, height);
		}

		public void draw(Graphics2D g2) {
			AffineTransform at = g2.getTransform();
			translate(g2, boxCenterX, boxCenterY);
			g2.rotate(tireRot);

			fillRect(g2, -boxWidth / 2, -boxHeight / 2, boxWidth, boxHeight);

			g2.setTransform(at);
		}
	}

	Box[] body = { // car components
	        new Box(0.0, 0.0, 1.0, 0.4), // main body
	        new Box(0.3, 0.0, 0.05, 0.8), // front axle
	        new Box(-0.3, 0.0, 0.05, 0.8) // rear axle
	};

	Tire[] frontTires = { // car components
	        new Tire(0.3, 0.4, 0.2, 0.15), // front left tire
	        new Tire(0.3, -0.4, 0.2, 0.15) // front right tire
	};

	Box[] rearTires = { // car components
	        new Box(-0.3, 0.4, 0.2, 0.15), // rear left tire
	        new Box(-0.3, -0.4, 0.2, 0.15) // rear right tire
	};

	@SuppressWarnings("serial")
	class Trail extends Point2D.Double {
		double rot;
		Color c;

		Trail(double x, double y, double rot, Color c) {
			super(x, y);
			this.rot = rot;
			this.c = c;
		}

		public void draw(Graphics2D g2) {
			drawDebugCross(g2, x, y, rot, 0.3, c);
		}
	}

	/** Inches per second */
	private double speed;

	private ArrayList<Trail> trails = new ArrayList<>();

	public Car(double posX, double posY, double width, double height, double rot, Color color) {
		super(posX, posY, width, height, rot, color);
	}

	public Point2D.Double getNosePosition() {
		Point2D.Double nosePos = ChMath.rotatePoint(0.5 * width, 0, 0, 0, rot);
		nosePos.x += centerX;
		nosePos.y += centerY;
		return nosePos;
	}

	@Override
	public void draw(Graphics2D g2) {
		switch (drawState) {
			case TRANSPARENT:
				g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));

			case OPAQUE:
				g2.setColor(color);

				// draw trails
				for (Trail trail : trails) {
					trail.draw(g2);
				}

				AffineTransform at = g2.getTransform();
				translate(g2, centerX, centerY);
				g2.rotate(rot);

				for (Box box : body) {
					box.draw(g2);
				}

				for (Box box : rearTires) {
					box.draw(g2);
				}

				for (Tire tire : frontTires) {
					tire.draw(g2);
				}

				g2.setTransform(at);

				// car orientation marker
				// drawDebugCross(g2, centerX, centerY, rot, 10, Color.YELLOW);

				// draw camera fov
				final double camFovSide1 = rot + Camera.pan - Camera.carCamFov / 2.0;
				final double camFovSide2 = rot + Camera.pan + Camera.carCamFov / 2.0;
				DoubleGraphics2D.drawLine(g2, centerX, centerY,
				        centerX + Camera.carCamDepthOfField * Math.cos(camFovSide1),
				        centerY + Camera.carCamDepthOfField * Math.sin(camFovSide1));
				DoubleGraphics2D.drawLine(g2, centerX, centerY,
				        centerX + Camera.carCamDepthOfField * Math.cos(camFovSide2),
				        centerY + Camera.carCamDepthOfField * Math.sin(camFovSide2));

				// draw distance sensor fov
				final double distFovSide1 = rot - DistanceSensor.carDistFov / 2.0;
				final double distFovSide2 = rot + DistanceSensor.carDistFov / 2.0;
				Point2D.Double nosePos = getNosePosition();
				DoubleGraphics2D.drawLine(g2, nosePos.x, nosePos.y,
				        nosePos.x + DistanceSensor.MAX_DIST * Math.cos(distFovSide1),
				        nosePos.y + DistanceSensor.MAX_DIST * Math.sin(distFovSide1));
				DoubleGraphics2D.drawLine(g2, nosePos.x, nosePos.y,
				        nosePos.x + DistanceSensor.MAX_DIST * Math.cos(distFovSide2),
				        nosePos.y + DistanceSensor.MAX_DIST * Math.sin(distFovSide2));

				// draw distance measurement representation
				DoubleGraphics2D.drawLine(g2, nosePos.x, nosePos.y,
				        nosePos.x + distanceSensor.getDistanceMeasurement() * Math.cos(rot),
				        nosePos.y + distanceSensor.getDistanceMeasurement() * Math.sin(rot));

			case HIDDEN:
			default:
		}
	}

	public void update() {

		translateAndRotate();

		if (AppData.MAX_NUM_TIRE_TRAILS > 0) {
			createTrailMarkers();
		}

		camera.doScan(centerX, centerY, rot);

		Point2D.Double nosePos = getNosePosition();
		distanceSensor.update(nosePos.x, nosePos.y, rot);

	}

	int count = 0;

	private void createTrailMarkers() {
		if (count++ % (ChApplication.Attribute.updatesPerSecond / 10) == 0) {
			Point2D.Double transformedBoxCoords = new Point2D.Double();
			int trailCount = 0;
			for (Tire tire : frontTires) {
				transformedBoxCoords = tire.calculatePosInWorld();
				trails.add(new Trail(transformedBoxCoords.x, transformedBoxCoords.y, tire.tireRot + rot, Color.GREEN));
				trailCount++;
			}

			for (Box tire : rearTires) {
				transformedBoxCoords = tire.calculatePosInWorld();
				trails.add(new Trail(transformedBoxCoords.x, transformedBoxCoords.y, rot, Color.BLUE));
				trailCount++;
			}

			if (trails.size() > AppData.MAX_NUM_TIRE_TRAILS) {
				for (int i = 0; i < trailCount; i++) {
					trails.remove(0);
				}
			}
		}
	}

	private void translateAndRotate() {

		/*
		 * angle used to turn car = front tire visual angle divided by
		 * magicFactor1
		 */
		double magicFactor1 = 2.1;
		/*
		 * higher factor causes more rotation: kick rear more to the outside and
		 * front to inside of turn
		 */
		double magicFactor2 = 2;

		// position delta due to speed and wheel angle
		double posDelta = speed / ChApplication.Attribute.updatesPerSecond;
		double posDeltaX = posDelta * Math.cos(frontTires[0].tireRot / magicFactor1 + rot);
		double posDeltaY = posDelta * Math.sin(frontTires[0].tireRot / magicFactor1 + rot);

		// old rear tire position
		Point2D.Double oldRearTirePos = rearTires[0].calculateAxlePosInWorld();

		// update car position in world
		centerX += posDeltaX;
		centerY += posDeltaY;

		// update rotation
		Point2D.Double newFrontTireCoords = frontTires[0].calculateAxlePosInWorld();
		double absRotDeltaY = newFrontTireCoords.y - oldRearTirePos.y;
		double absRotDeltaX = newFrontTireCoords.x - oldRearTirePos.x;

		rot += (Math.atan2(absRotDeltaY, absRotDeltaX) - rot) * magicFactor2;

		while (rot <= -Math.PI) {
			rot += 2 * Math.PI;
		}

		while (rot > Math.PI) {
			rot -= 2 * Math.PI;
		}
	}

}
