package autocar.objects;

import java.awt.geom.Point2D;

import autocar.AppData;

public class DistanceSensor {

	public static final double carDistFov = 30 * (Math.PI / 180.0);
	public static final double MAX_DIST = 12 * 5;

	/** Inches */
	private double distanceMeasurement;

	DistanceSensor() {
	}

	public void update(double carX, double carY, double carRot) {
		double entityDeclination;
		double minDistSq = Double.MAX_VALUE, minDist;
		double curDistSq;

		Point2D.Double nosePos = new Point2D.Double(carX, carY);
		for (Entity entity : AppData.getInstance().entities.values()) {

			// filter using sensor FOV
			entityDeclination = Math.atan2(entity.centerY - nosePos.y, entity.centerX - nosePos.x) - carRot;
			if (entityDeclination > -carDistFov / 2.0 && entityDeclination < carDistFov / 2.0) {

				// calculate distance squared (cheaper)
				curDistSq = nosePos.distanceSq(entity.centerX, entity.centerY);
				if (curDistSq < minDistSq) {
					minDistSq = curDistSq;
				}

			}

		}
		minDist = Math.sqrt(minDistSq);
		if (minDist > MAX_DIST) {
			minDist = MAX_DIST;
		}
		distanceMeasurement = minDist;
	}

	/** Inches */
	public int getDistanceMeasurement() {
		return (int) distanceMeasurement;
	}

}
