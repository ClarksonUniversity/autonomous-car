package autocar;

import java.util.HashMap;

import autocar.objects.Entity;

public class AppData {

	private static AppData instance;

	public static final String BUTTON_EXIT = "exit";

	/** Sets the base zoom multiplier for the camera */
	public static final double coordinateScale = 10;
	/** Increases the pixel drawing resolution by this factor */
	public static final double coordinateResolution = 100;

	public static final double MAX_WHEEL_ANGLE = 45 * (Math.PI / 180.0);

	public static int MAX_NUM_TIRE_TRAILS = 200;

	public static final boolean manual_car_control = false;
	public static final double manual_control_deadzone = 0.1;
	public static final double MAX_CAR_SPEED = 20;

	public final HashMap<String, Entity> entities;

	public static AppData getInstance() {
		if (instance == null) {
			instance = new AppData();
		}
		if (!instance.isValid()) {
			System.err.println("Invalid App state.");
			System.exit(0);
		}
		return instance;
	}

	private AppData() {
		entities = new HashMap<>();
	}

	public boolean isValid() {
		return true;
	}

}
