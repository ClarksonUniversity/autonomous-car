package autocar.ui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import murphycd.engine.util.Drawable;

public class Element implements Drawable {

	protected Rectangle region;

	protected boolean isFocused = false;

	protected Color unfocusedColor;
	protected Color focusedColor;

	protected Element(Rectangle bounds) {
		region = bounds;
		unfocusedColor = new Color(0xFFFFFF);
		focusedColor = new Color(0x555555);
	}

	@Override
	public void draw(Graphics2D g2) {
		final Color previous = g2.getColor();

		g2.setColor(isFocused ? focusedColor : unfocusedColor);
		g2.fillRect(region.x, region.y, region.width, region.height);

		g2.setColor(previous);
	}

	public void setFocused(boolean focused) {
		this.isFocused = focused;
	}

	public boolean isFocused() {
		return isFocused;
	}

	public boolean overlaps(Point point) {
		return region.contains(point);
	}

}