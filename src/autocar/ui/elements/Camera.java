package autocar.ui.elements;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import autocar.AppData;
import autocar.objects.Entity;
import autocar.ui.Element;
import murphycd.engine.util.ChMath;

public class Camera extends Element {

	public static final int carCameraViewportWidth = 316;
	public static final int carCameraViewportHeight = 208;
	public static final double carCamFov = 60 * (Math.PI / 180.0);
	public static final double carCamFovV = 40 * (Math.PI / 180.0);
	/** (6 ft)*(12 in/ft) = 216 inches */
	public static final int carCamDepthOfField = 12 * 9;

	public static double pan = 0.0 * (Math.PI / 180.0);

	public static double carCamZ = 5;

	// TODO if transformedEntities weren't static, Camera#draw would find it has
	// a size of zero even though Camera#update populates the ArrayList. Why
	// does this happen?
	private static ArrayList<Block> transformedEntities;

	public class Block {
		Rectangle2D.Double bounds;
		Color color;

		Block(Rectangle2D.Double bounds, Color color) {
			this.bounds = bounds;
			this.color = color;
		}

		public Point2D.Double getCenter() {
			return new Point2D.Double(bounds.getCenterX(), bounds.getCenterY());
		}

		public double getCenterX() {
			return bounds.getCenterX();
		}

		public double getCenterY() {
			return bounds.getCenterY();
		}

		public double getWidth() {
			return bounds.getWidth();
		}

		public double getHeight() {
			return bounds.getHeight();
		}

		public Color getColor() {
			return color;
		}
	}

	public Camera(Rectangle bounds) {
		super(bounds);

		// element border
		focusedColor = Color.BLACK;
		// background
		unfocusedColor = Color.WHITE;

		transformedEntities = new ArrayList<>();
	}

	public List<Block> getCameraBlocksReversed() {
		transformedEntities.sort(new Comparator<Block>() {

			@Override
			public int compare(Block block1, Block block2) {
				// sort back to front
				return block1.bounds.getHeight() < block2.bounds.getHeight() ? -1 : 1;
			}

		});
		return transformedEntities;
	}

	@Override
	public void draw(Graphics2D g2) {

		// viewport background
		g2.setColor(unfocusedColor);
		g2.fillRect(region.x, region.y, region.width, region.height);

		// viewport border
		g2.setColor(focusedColor);
		g2.drawRect(region.x, region.y, region.width, region.height);

		// objects in scene
		int x, y, w, h;
		g2.clipRect(region.x, region.y, region.width, region.height);
		for (Block entity : getCameraBlocksReversed()) {
			x = (int) (region.x + entity.bounds.x * region.width);
			y = (int) (region.y + entity.bounds.y * region.height);
			w = (int) (region.x + entity.bounds.width * region.width);
			h = (int) (region.x + entity.bounds.height * region.width);
			g2.setColor(entity.color);
			g2.fillRect(x, y, w, h);
		}
		g2.setClip(null);
	}

	public void doScan(double carX, double carY, double carRot) {

		Rectangle2D.Double entityBounds;
		Point2D.Double transformedPos;
		double targetTheta1, targetTheta2, targetPhi1, targetPhi2;
		double targetDistXY;

		double normalizedFovTheta1, normalizedFovTheta2, normalizedFovPhi1, normalizedFovPhi2;

		transformedEntities.clear();
		for (Entity entity : AppData.getInstance().entities.values()) {
			entityBounds = entity.getBounds();

			// calculate x projection onto camera plane
			transformedPos = ChMath.rotatePoint(entityBounds.getCenterX() - carX, entityBounds.getCenterY() - carY, 0,
			        0, -carRot - pan);
			if (transformedPos.x < 0) {
				continue;
			}
			targetTheta1 = Math.atan2(transformedPos.y + entityBounds.getWidth() / 2.0, transformedPos.x)
			        + carCamFov / 2.0;
			targetTheta2 = Math.atan2(transformedPos.y - entityBounds.getWidth() / 2.0, transformedPos.x)
			        + carCamFov / 2.0;
			normalizedFovTheta1 = targetTheta1 / carCamFov;
			normalizedFovTheta2 = targetTheta2 / carCamFov;

			// calculate y projection onto camera plane
			targetDistXY = transformedPos.distance(0, 0);
			if (targetDistXY > Camera.carCamDepthOfField) {
				continue;
			}
			targetPhi1 = Math.atan2(-carCamZ, targetDistXY) + carCamFovV / 2.0;
			targetPhi2 = Math.atan2(carCamZ, targetDistXY) + carCamFovV / 2.0;
			normalizedFovPhi1 = targetPhi1 / carCamFovV;
			normalizedFovPhi2 = targetPhi2 / carCamFovV;

			transformedEntities.add(new Block(new Rectangle2D.Double(normalizedFovTheta2, normalizedFovPhi1,
			                normalizedFovTheta1 - normalizedFovTheta2, normalizedFovPhi2 - normalizedFovPhi1),
			        entity.getColor()));
		}
	}

}
