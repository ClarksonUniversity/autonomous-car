package autocar.ui;

import static murphycd.engine.core.ChApplication.Attribute.getScaledWidth;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.HashMap;

import autocar.AppData;
import autocar.ui.elements.Camera;
import murphycd.engine.util.ButtonClickHandler;
import murphycd.engine.util.ButtonClickHandler.ButtonClickEvent;
import murphycd.engine.util.Drawable;
import murphycd.engine.util.Input.MouseInputListener;

/**
 * Visuals for User Interface
 */
public final class Gui implements Drawable, MouseInputListener {

	private final int TITLE_BAR_HEIGHT = 20;

	private ButtonClickHandler buttonClickHandler;

	private TitleBar titleBar;
	private ExitButton exitButton;

	private HashMap<String, Element> elements;

	private class TitleBar extends Element implements Drawable {

		private TitleBar(int height) {
			super(new Rectangle(0, 0, (int) (getScaledWidth() - height), height));
			focusedColor = Color.WHITE;
		}

		@Override
		public void draw(Graphics2D g) {
			// TODO make title bar drag whole window, unhide
			// if (isFocused) {
			// final Color previous = g.getColor();
			// g.setColor(focusedColor);
			// g.fillRect(region.x, region.y, region.width, region.height);
			// g.setColor(previous);
			// }
		}
	}

	private class ExitButton extends Element implements Drawable {

		/**
		 * Actual exit button image (cross) is drawn at the region width less
		 * any insets on all sides, where insets are calculated as the button
		 * width times the following factor.
		 */
		private final double paddingPercentage = 0.2;

		private final Color unfocusedTextColor = new Color(0x555555);
		private final Color focusedTextColor = new Color(0xFFFFFF);

		/** Bounds for "X" mark */
		private int x1, y1, x2, y2;

		/**
		 * Create a new Exit Button (square) in the upper-right of the screen
		 * with the specified side length.
		 */
		private ExitButton(int size) {
			super(new Rectangle((int) (getScaledWidth() - size + 0.5), 0, size, size));

			/**
			 * Exit button color during mouse hover.
			 */
			focusedColor = Color.RED;

			calculatePadding(size);
		}

		private void calculatePadding(int size) {
			region.setBounds((int) (getScaledWidth() - size + 0.5) - 1, 1, size, size);

			// bounds for "X" mark
			final double x = region.getX() + (size * paddingPercentage);
			final double y = region.getY() + (size * paddingPercentage);
			final double w = size * (1 - 2 * paddingPercentage);
			final double h = size * (1 - 2 * paddingPercentage);
			x1 = (int) (x + 0.5);
			y1 = (int) (y + 0.5);
			x2 = (int) (x + w + 0.5);
			y2 = (int) (y + h + 0.5);
		}

		@Override
		public void draw(Graphics2D g) {
			final Color previous = g.getColor();
			
			if (isFocused) {
				g.setColor(Color.RED);
				g.fillRect(region.x, region.y, region.width, region.height);
			}

			g.setColor(isFocused ? focusedTextColor : unfocusedTextColor);
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1, y2, x2, y1);
			g.setColor(previous);
		}
	}

	public Gui(ButtonClickHandler buttonClickHandler) {

		this.buttonClickHandler = buttonClickHandler;

		titleBar = new TitleBar(TITLE_BAR_HEIGHT);
		exitButton = new ExitButton(TITLE_BAR_HEIGHT);
		elements = new HashMap<>();
		elements.put(AppData.BUTTON_EXIT, exitButton);
		elements.put("titleBar", titleBar);

		// create other gui elements
		elements.put("cameraView",
		        new Camera(new Rectangle(20, 20, Camera.carCameraViewportWidth, Camera.carCameraViewportHeight)));
	}

	private boolean isFocused() {
		boolean focused = false;
		for (Element element : elements.values()) {
			focused |= element.isFocused();
		}
		return focused;
	}

	@Override
	public void draw(Graphics2D g2) {
		for (Element element : elements.values()) {
			element.draw(g2);
		}
	}

	@Override
	public boolean mouseClicked(MouseEvent e) {
		boolean processed = false;
		// button click events
		if (e.getButton() == MouseEvent.BUTTON1) {
			if (exitButton.overlaps(e.getPoint())) {
				buttonClickHandler.buttonClicked(new ButtonClickEvent(AppData.BUTTON_EXIT));
				processed = true;
			}
		}
		return processed;
	}

	@Override
	public boolean mousePressed(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseReleased(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseEntered(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseMoved(MouseEvent e) {
		// TODO call onFocus event in Element instead of current implementation
		for (Element element : elements.values()) {
			if (element.overlaps(e.getPoint())) {
				element.setFocused(true);
				return true;
			} else {
				element.setFocused(false);
			}
		}
		return false;
	}

	@Override
	public boolean mouseDragged(MouseEvent e) {
		return isFocused();
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent e) {
		return isFocused();
	}
}
