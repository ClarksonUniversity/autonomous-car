package murphycd.engine.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;

import autocar.AppData;

public class DoubleGraphics2D {

	static final double S = AppData.coordinateResolution;

	/**
	 * Scaled version of
	 * {@link java.awt.Graphics2D#fillOval(int, int, int, int)}
	 */
	public static void fillOval(Graphics2D g, double x, double y, double width, double height) {
		g.fillOval((int) (x * S), (int) (y * S), (int) (width * S), (int) (height * S));
	}

	/**
	 * Scaled version of
	 * {@link java.awt.Graphics2D#fillRect(int, int, int, int)}
	 */
	public static void fillRect(Graphics2D g, double x, double y, double width, double height) {
		g.fillRect((int) (x * S), (int) (y * S), (int) (width * S), (int) (height * S));
	}

	/**
	 * Scaled version of
	 * {@link java.awt.Graphics2D#drawRect(int, int, int, int)}
	 */
	public static void drawRect(Graphics2D g, double x, double y, double width, double height) {
		g.fillRect((int) (x * S), (int) (y * S), (int) (width * S), (int) (height * S));
	}

	/**
	 * Scaled version of
	 * {@link java.awt.Graphics2D#drawLine(int, int, int, int)}
	 */
	public static void drawLine(Graphics2D g, double x1, double y1, double x2, double y2) {
		Stroke oldStroke = g.getStroke();
		g.setStroke(new BasicStroke((float) (S / 10)));
		g.drawLine((int) (x1 * S), (int) (y1 * S), (int) (x2 * S), (int) (y2 * S));
		g.setStroke(oldStroke);
	}

	/**
	 * Scaled version of {@link java.awt.Graphics2D#translate(double, double)}
	 */
	public static void translate(Graphics2D g, double tx, double ty) {
		g.translate(tx * S, ty * S);
	}

	public static void drawDebugCross(Graphics2D g2, double x, double y, double rot, double length, Color color) {
		AffineTransform at = g2.getTransform();
		translate(g2, x, y);
		g2.rotate(rot);
		Color oldColor = g2.getColor();
		g2.setColor(color);
		Stroke oldStroke = g2.getStroke();
		g2.setStroke(new BasicStroke((float) (S / 10)));
		g2.drawLine(0, 0, (int) (-length * S), 0);
		g2.drawLine(0, (int) (length * S), 0, (int) (-length * S));
		g2.setColor(new Color(-1 - color.getRGB()));
		g2.drawLine((int) (length * 3 * S), 0, 0, 0);
		g2.setStroke(oldStroke);
		g2.setColor(oldColor);
		g2.setTransform(at);
	}

}
