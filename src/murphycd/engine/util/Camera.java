package murphycd.engine.util;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

import autocar.AppData;
import murphycd.engine.util.Input.MouseInputListener;

/**
 * Listener that can be attached to a Component to implement Zoom and Pan
 * functionality.
 *
 * @author Sorin Postelnicu (Pan and Zoom), modified by Clarkson University 2018
 * @since Jul 14, 2009
 */
public final class Camera implements MouseInputListener {
	public static final int DEFAULT_MIN_ZOOM_LEVEL = -10;
	public static final int DEFAULT_MAX_ZOOM_LEVEL = 22;
	public static final double DEFAULT_ZOOM_MULTIPLICATION_FACTOR = 1.2;

	private Component targetComponent;

	private double initScale = AppData.coordinateScale;
	private int zoomLevel = 0;
	private int minZoomLevel = DEFAULT_MIN_ZOOM_LEVEL;
	private int maxZoomLevel = DEFAULT_MAX_ZOOM_LEVEL;
	private double zoomMultiplicationFactor = DEFAULT_ZOOM_MULTIPLICATION_FACTOR;

	private Point dragStartScreen;
	private Point dragEndScreen;
	private static AffineTransform coordTransform = new AffineTransform();
	private AffineTransform oldTransform = new AffineTransform();

	private boolean init = true;

	public Camera(Component targetComponent) {
		this.targetComponent = targetComponent;

		targetComponent.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
	}

	public Camera(Component targetComponent, int minZoomLevel, int maxZoomLevel, double zoomMultiplicationFactor) {
		this.targetComponent = targetComponent;
		this.minZoomLevel = minZoomLevel;
		this.maxZoomLevel = maxZoomLevel;
		this.zoomMultiplicationFactor = zoomMultiplicationFactor;
	}

	@Override
	public boolean mouseClicked(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mousePressed(MouseEvent e) {
		dragStartScreen = e.getPoint();
		dragEndScreen = null;
		return true;
	}

	@Override
	public boolean mouseReleased(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseEntered(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseExited(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseMoved(MouseEvent e) {
		return false;
	}

	@Override
	public boolean mouseDragged(MouseEvent e) {
		moveCamera(e);
		return true;
	}

	@Override
	public boolean mouseWheelMoved(MouseWheelEvent e) {
		zoomCamera(e);
		return true;
	}

	private void moveCamera(MouseEvent e) {
		try {
			dragEndScreen = e.getPoint();
			Point2D.Float dragStart = transformPoint(dragStartScreen);
			Point2D.Float dragEnd = transformPoint(dragEndScreen);
			double dx = dragEnd.getX() - dragStart.getX();
			double dy = dragEnd.getY() - dragStart.getY();
			coordTransform.translate(dx, dy);
			dragStartScreen = dragEndScreen;
			dragEndScreen = null;
		} catch (NoninvertibleTransformException ex) {
			ex.printStackTrace();
		}
	}

	private void zoomCamera(MouseWheelEvent e) {
		try {
			int wheelRotation = e.getWheelRotation();
			Point p = e.getPoint();
			if (wheelRotation > 0) {
				if (zoomLevel < maxZoomLevel) {
					zoomLevel++;
					Point2D p1 = transformPoint(p);
					coordTransform.scale(1 / zoomMultiplicationFactor, 1 / zoomMultiplicationFactor);
					Point2D p2 = transformPoint(p);
					coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
				}
			} else {
				if (zoomLevel > minZoomLevel) {
					zoomLevel--;
					Point2D p1 = transformPoint(p);
					coordTransform.scale(zoomMultiplicationFactor, zoomMultiplicationFactor);
					Point2D p2 = transformPoint(p);
					coordTransform.translate(p2.getX() - p1.getX(), p2.getY() - p1.getY());
				}
			}
		} catch (NoninvertibleTransformException ex) {
			ex.printStackTrace();
		}
	}

	/** Map world space -> screen space */
	public static Point2D.Float transformPoint(Point p1) throws NoninvertibleTransformException {
		AffineTransform inverse = coordTransform.createInverse();
		Point2D.Float p2 = new Point2D.Float();
		inverse.transform(p1, p2);
		return p2;
		// TODO rename and create opposite method
	}

	public void setInitScale(double initScale) {
		this.initScale = initScale;
	}

	public int getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(int zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	public AffineTransform getCoordTransform() {
		return coordTransform;
	}

	public void setCoordTransform(AffineTransform coordTransform) {
		Camera.coordTransform = coordTransform;
	}

	/**
	 * Setup world space to screen space projection using current pan and zoom
	 */
	public void setProjection(Graphics g1) {
		Graphics2D g = (Graphics2D) g1;
		if (init) {
			// save unmodified viewport
			oldTransform = g.getTransform();
			// Initialize the viewport by moving the origin to the center of the window
			init = false;
			Dimension d = targetComponent.getSize();
			int xc = d.width / 2;
			int yc = d.height / 2;
			g.translate(xc, yc);
			g.scale(initScale / DoubleGraphics2D.S, initScale / DoubleGraphics2D.S);
			// Save the viewport to be updated by the Camera
			setCoordTransform(g.getTransform());
		} else {
			// Restore the viewport after it was updated by the Camera
			g.setTransform(getCoordTransform());
		}

	}

	public void resetProjection(Graphics g) {
		((Graphics2D) g).setTransform(oldTransform);
	}

}
