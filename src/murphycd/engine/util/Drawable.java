package murphycd.engine.util;

public interface Drawable {

	public enum State {
		OPAQUE, TRANSPARENT, HIDDEN
	}

	void draw(java.awt.Graphics2D g2);

}
