package murphycd.engine.util;

import java.awt.geom.Point2D;

public class ChMath {

	public static Point2D.Double rotatePoint(double x, double y, double originX, double originY, double angle) {
		Point2D.Double newPoint = new Point2D.Double();
		newPoint.x = originX + x * Math.cos(angle) - y * Math.sin(angle);
		newPoint.y = originY + x * Math.sin(angle) + y * Math.cos(angle);
		return newPoint;
	}
	
}
