package murphycd.engine.util;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;

public final class Input implements MouseListener, MouseMotionListener, MouseWheelListener {

	/**
	 * Accepts mouse input.<br>
	 * Implementations should return true to indicate the event has been
	 * processed, or false to continue propagating the event to all listeners.
	 * 
	 * @see {@link java.awt.event.MouseListener}
	 * @see {@link java.awt.event.MouseMotionListener}
	 * @see {@link java.awt.event.MouseWheelListener}
	 */
	public interface MouseInputListener {

		/** @see {@link java.awt.event.MouseListener#mouseClicked(MouseEvent)} */
		public boolean mouseClicked(MouseEvent event);

		/** @see {@link java.awt.event.MouseListener#mouseEntered(MouseEvent)} */
		public boolean mouseEntered(MouseEvent event);

		/** @see {@link java.awt.event.MouseListener#mouseExited(MouseEvent)} */
		public boolean mouseExited(MouseEvent event);

		/** @see {@link java.awt.event.MouseListener#mousePressed(MouseEvent)} */
		public boolean mousePressed(MouseEvent event);

		/** @see {@link java.awt.event.MouseListener#mouseReleased(MouseEvent)} */
		public boolean mouseReleased(MouseEvent event);

		/** @see {@link java.awt.event.MouseMotionListener#mouseMoved(MouseEvent)} */
		public boolean mouseMoved(MouseEvent event);

		/** @see {@link java.awt.event.MouseMotionListener#mouseDragged(MouseEvent)} */
		public boolean mouseDragged(MouseEvent event);

		/** @see {@link java.awt.event.MouseWheelListener#mouseWheelMoved(MouseWheelEvent)} */
		public boolean mouseWheelMoved(MouseWheelEvent event);
	}

	private LinkedList<MouseInputListener> registered;

	public Input(Component targetComponent) {
		registered = new LinkedList<>();

		targetComponent.addMouseListener(this);
		targetComponent.addMouseMotionListener(this);
		targetComponent.addMouseWheelListener(this);
	}

	/**
	 * Registers a listener to receive input in the order they were added.
	 */
	public void addMouseInputListener(MouseInputListener listener) {
		registered.add(listener);
	}

	/**
	 * Unregisters a listener while preserving the order of the remaining
	 * listener chain.
	 */
	public void removeMouseInputListener(MouseInputListener listener) {
		registered.remove(listener);
	}

	private void processMouseEvent(String name, Object event) {
		for (MouseInputListener listener : registered) {
			try {
				Method method = listener.getClass().getDeclaredMethod(name, event.getClass());
				if ((boolean) method.invoke(listener, event)) {
					break;
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
			        | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent event) {
		processMouseEvent("mouseWheelMoved", event);
	}

	@Override
	public void mouseDragged(MouseEvent event) {
		processMouseEvent("mouseDragged", event);
	}

	@Override
	public void mouseMoved(MouseEvent event) {
		processMouseEvent("mouseMoved", event);
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		processMouseEvent("mouseClicked", event);
	}

	@Override
	public void mouseEntered(MouseEvent event) {
		processMouseEvent("mouseEntered", event);
	}

	@Override
	public void mouseExited(MouseEvent event) {
		processMouseEvent("mouseExited", event);
	}

	@Override
	public void mousePressed(MouseEvent event) {
		processMouseEvent("mousePressed", event);
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		processMouseEvent("mouseReleased", event);
	}

}
